import os
from pathlib import Path

import pandas as pd
from bokeh.io import show, save
from bokeh.models import ColumnDataSource
from bokeh.plotting import figure
from bokeh.palettes import Category10
from bokeh.transform import factor_cmap

URL = 'https://data.mesaaz.gov/api/views/4k95-x7aw/rows.csv'
DATA_DIR = Path.cwd().parent.joinpath('data')

weekdays = {'Monday': 0, 'Tuesday': 1, 'Wednesday': 2, 'Thursday': 3,
            'Friday': 4, 'Saturday': 5, 'Sunday': 6}

if 'mesa.csv' not in os.listdir(DATA_DIR):
    print('Downloading')
    mesa = pd.read_csv(URL)
    mesa.to_csv(DATA_DIR.joinpath('mesa.csv'))

mesa = pd.read_csv(DATA_DIR.joinpath('mesa.csv'))
mesa.columns = [c.title().replace(' ', '_') for c in mesa.columns]
mesa = mesa.drop('Unnamed:_0', axis=1)
mesa['Creation_Datetime'] = pd.to_datetime(mesa.Creation_Datetime)

accidents = (mesa[mesa.Event_Type_Description.str.contains('ACCIDENT')]
             .copy()
             .reset_index())

accidents['DOW_Alpha'] = accidents.Creation_Datetime.dt.day_name()

by_day = \
    accidents.groupby('DOW_Alpha')['Event_Type_Description'].value_counts()

grouped_by_day = by_day.unstack().reset_index().fillna(0)
grouped_by_day['DOW_Numeric'] = grouped_by_day.DOW_Alpha.map(weekdays)
grouped_by_day = grouped_by_day.sort_values(by='DOW_Numeric')

dow = grouped_by_day['DOW_Alpha'].to_list()
accident = grouped_by_day['ACCIDENT'].to_list()
accident_with_injury = grouped_by_day['ACCIDENT W/INJURIES'].to_list()
fatal_accident = grouped_by_day['FATAL ACCIDENT'].to_list()
hit_and_run_accident = grouped_by_day['HIT and RUN ACCIDENT'].to_list()
hit_and_run_with_injury = \
    grouped_by_day['HIT and RUN ACCIDENT/INJURY'].to_list()

to_plot = {
    'dow': dow,
    'accident': accident,
    'accident_with_injury': accident_with_injury,
    'fatal_accident': fatal_accident,
    'hit_and_run_accident': hit_and_run_accident,
    'hit_and_run_with_injury': hit_and_run_with_injury
}

cds = ColumnDataSource(to_plot)

x_range = [i for i in to_plot['dow']]
stack = [i for i in to_plot.keys() if i != 'dow']
labels = [i.title().replace('_', ' ') for i in stack]

cmap = factor_cmap('dow',
                   palette=Category10[len(to_plot['dow'])],
                   factors=to_plot['dow'])

p = figure(plot_width=1400, plot_height=1_000, x_range=x_range,
           title='Mesa Accidents by Day of Week',
           x_axis_label='Day of Week', y_axis_label='Number of Accidents',
           tools='hover', tooltips='$name @$name',
           toolbar_location=None)

p.vbar_stack(stackers=stack, x='dow', legend_label=labels, source=cds,
             width=.95, line_color='white', fill_color=Category10[len(labels)])

p.y_range.start = 0
p.x_range.range_padding = 0.025
p.xgrid.grid_line_color = None
p.axis.minor_tick_line_color = None
p.legend.location = 'top_center'
p.legend.orientation = 'horizontal'
p.title.text_font_size = '24px'
p.yaxis.axis_label_text_font_size = '18px'
p.xaxis.axis_label_text_font_size = '18px'

save(p, filename=DATA_DIR.parent.joinpath('plots').joinpath(
    '03_bar_mesa_accidents.html'), title='Mesa Accidents')
show(p, browser='firefox')
