from pathlib import Path

import numpy as np
import pandas as pd
from scipy.stats.kde import gaussian_kde
from bokeh.io import output_file, show
from bokeh.models import ColumnDataSource, FixedTicker
from bokeh.plotting import figure
from bokeh.palettes import Category20

DATA_DIR = Path.cwd().parent.joinpath('data')


def ridge(category, data, scale=20):
    return list(zip([category] * len(data), scale * data))


temps = pd.read_csv(DATA_DIR.joinpath('phoenix_maximum_daily_temps.csv'),
                    index_col=0)
temps = temps.replace('M', np.NAN).astype(np.float)

has_na = [col for col in temps.columns if temps[col].hasnans]
medians = {col: temps[col].median() for col in has_na}
temps = temps.fillna(medians)
cols = [*temps.columns]
colors = [*Category20[len(cols)]]

min_temp,  max_temp = temps.min().min(), temps.max().max()

x = np.linspace(start=(min_temp - 5), stop=(max_temp + 5), num=1_000)
cds = ColumnDataSource(data={'x': x})

p = figure(y_range=cols, x_range=(55, 131), plot_width=1_200,
           plot_height=1_200, toolbar_location=None,
           x_axis_label='Degrees Farenheit', y_axis_label='Month',
           title='Phoenix Max Temperatures by Month 1919-2019')

for i, month in enumerate(temps.columns):
    pdf = gaussian_kde(temps[month])
    y = ridge(month, pdf(x))
    cds.add(y, month)
    p.patch(x='x', y=month, color=colors[i], line_color='lightslategray',
            alpha=.5, source=cds, legend_label=month)

p.outline_line_color = None

p.xaxis.ticker = FixedTicker(
    ticks=[*range(int(min_temp-10), int(max_temp + 20), 5)])

p.ygrid.grid_line_color = None
p.xgrid.ticker = p.xaxis.ticker

p.axis.minor_tick_line_color = None
p.axis.major_tick_line_color = None
p.axis.axis_line_color = None
p.title.text_font_size = '24px'
p.yaxis.axis_label_text_font_size = '18px'
p.xaxis.axis_label_text_font_size = '18px'

p.y_range.range_padding = 0.35
output_file(DATA_DIR.parent.joinpath('plots').joinpath(
    '06_multidistrobution_plot.html'), title='Phoenix Max Temps')
show(p)
