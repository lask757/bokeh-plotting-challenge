from pathlib import Path

import pandas as pd
from bokeh.io import output_file, show
from bokeh.palettes import Category10
from bokeh.models import ColumnDataSource
from bokeh.plotting import figure
from bokeh.transform import factor_cmap
from bokeh.layouts import gridplot

DATA_DIR = Path.cwd().parent.joinpath('data')

if __name__ == '__main__':
    iris = pd.read_csv(DATA_DIR.joinpath('iris.csv'))
    iris['species'] = iris.species.apply(lambda x: x.split('-')[-1])
    features = iris.columns[:-1]

    cds = ColumnDataSource(iris)
    mapper = factor_cmap('species', palette=Category10[iris.species.nunique()],
                         factors=iris.species.unique())

    plots_to_create = []
    for i, feature_0 in enumerate(features, start=1):
        for feature_1 in features[i:]:
            plots_to_create.append(f'{feature_0}--{feature_1}')

    plots = []
    for plot in plots_to_create:
        x, y = plot.split('--')
        x_label = ' '.join(x.split('_')).title()
        y_label = ' '.join(y.split('_')).title()
        title = f'{x_label} * {y_label}'.title()
        p = figure(width=400, height=400, x_axis_label=x_label, title=title,
                   y_axis_label=y_label, tools='hover', toolbar_location=None,
                   tooltips=[(x_label, '$x'), (y_label, '$y'),
                             ('species', '@species')])
        p.circle(x=x, y=y, color=mapper, source=cds, alpha=.5, size=10)
        plots.append(p)

    to_plot = plots
    rows = []
    mid_point = len(plots) // 2
    for stop in range(mid_point, 0, -1):
        out = to_plot[:stop]
        rows.append(out)
        for i in out:
            to_plot.remove(i)

    out = gridplot(rows, toolbar_location=None)
    output_file(DATA_DIR.parent.joinpath('plots')
                .joinpath('05_multivariate_plot.html'), title='Iris Features')
    show(out, browser='firefox')
