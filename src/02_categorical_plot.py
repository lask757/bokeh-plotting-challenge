from pathlib import Path

import pandas as pd
from bokeh.io import show, output_file
from bokeh.models import ColumnDataSource
from bokeh.plotting import figure
from bokeh.palettes import Category10

DATA_DIR = Path.cwd().parent.joinpath('data')
CRIMES = ('ARSON', 'MOTOR VEHICLE THEFT', 'DRUG OFFENSE')

dtypes = {'INC NUMBER': object, 'UCR CRIME CATEGORY': object,
          '100 BLOCK ADDRESS': object, 'ZIP': float, 'PREMISE TYPE': object}

phx_crimes: pd.DataFrame = \
    pd.read_csv(DATA_DIR.joinpath('crime-data_crime-data_crimestat.csv'),
                dtype=dtypes, parse_dates=['OCCURRED ON', 'OCCURRED TO'])

phx_crimes.columns = \
    [col.lower().replace(' ', '_') for col in phx_crimes.columns]


phx_crimes = phx_crimes.dropna(subset=['occurred_on'])

crimes_df = (phx_crimes[phx_crimes.ucr_crime_category.isin(CRIMES)]
             .reset_index(drop=True).copy())

crimes_df['dow'] = crimes_df.occurred_on.dt.dayofweek
crimes_df['hour'] = crimes_df.occurred_on.dt.hour

filtered_by_dow = (crimes_df.groupby('dow').ucr_crime_category
                   .value_counts()
                   .unstack())


dow = filtered_by_dow.loc[:, 'ARSON'].index.to_list()
arson, gta, drug = [filtered_by_dow.loc[:, i].to_list() for i in CRIMES]

weekdays = {0: 'Monday', 1: 'Tuesday', 2: 'Wednesday', 3: 'Thursday',
            4: 'Friday', 5: 'Saturday', 6: 'Sunday'}

dow_name = [weekdays[i] for i in dow]

data_dict = {'dow': dow_name, 'arson': arson, 'vehicle_theft': gta,
             'drug_related': drug}

cds = ColumnDataSource(data_dict)

p = figure(x_axis_label='Day of week', y_axis_label='Number of Crimes',
           plot_height=1_000, plot_width=1400,
           title='Phoenix reported crimes by day of week',
           tools='hover', tooltips="$name @$name",
           toolbar_location=None, x_range=dow_name)

p.vbar_stack(['arson', 'vehicle_theft', 'drug_related'], x='dow',
             fill_color=Category10[3], source=cds, width=.95,
             line_color='white',
             legend_label=['Arson', 'Vehicle theft', 'Drug related'])

p.y_range.start = 0
p.x_range.range_padding = 0.025
p.xgrid.grid_line_color = None
p.axis.minor_tick_line_color = None
p.legend.location = 'top_left'
p.legend.orientation = 'horizontal'
p.title.text_font_size = '24px'
p.yaxis.axis_label_text_font_size = '18px'
p.xaxis.axis_label_text_font_size = '18px'

output_file(p, filename=DATA_DIR.parent.joinpath('plots').joinpath(
    '02_stacked_bar.html'), title='Phoenix Reported Crimes')
show(p, browser='firefox')
