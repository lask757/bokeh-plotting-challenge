from pathlib import Path
import re

import pandas as pd
from bokeh.io import output_file, show
from bokeh.palettes import Viridis
from bokeh.models import LinearColorMapper, ColorBar
from bokeh.plotting import figure

from move_bokeh_files import copy_files, check_if_files_exist

DATA_DIR = Path.cwd().parent.joinpath('data')
BOKEH_DIR = Path.home().joinpath('.bokeh').joinpath('data')
DATA_FILES = ('unemployment09.csv', 'US_Counties.csv')
STATE_NUM = 4,  # AZ


def clean_text(text: str) -> str:
    text = re.sub(r'[A-Za-z<>/]', '', text)
    text = text.replace(',', ', ')
    return text


def add_ending_charecter(text: str, ending_char: str = ',') -> str:
    return ' '.join([f'{word},' if word[-1] != ending_char
                     else word for word in text.strip().split()])


if __name__ == '__main__':
    to_move = \
        check_if_files_exist(files_to_check=DATA_FILES, data_dir=DATA_DIR)
    if not to_move:
        print('Moving data files')
        copy_files(src=BOKEH_DIR, dest=DATA_DIR, files=DATA_FILES)

    counties = pd.read_csv(DATA_DIR.joinpath(DATA_FILES[1]))

    unp_names = ('county_id', 'state_num', 'county_num', 'county_state',
                 'year', 'workforce', 'employed', 'unemployed', 'unp_rate')
    unemployment = pd.read_csv(DATA_DIR.joinpath(DATA_FILES[0]),
                               header=None, names=unp_names)
    az_counties = counties[counties['STATE num'] == STATE_NUM[0]].copy()
    az_ump = unemployment[unemployment.state_num == STATE_NUM[0]].copy()
    assert az_ump.shape[0] == az_counties.shape[0]
    az = pd.merge(left=az_counties, right=az_ump,
                  left_on='COUNTY num', right_on='county_num')

    az = az.loc[:, ['County Name', 'workforce', 'unp_rate', 'geometry']].copy()
    az['geometry'] = (az.geometry
                      .apply(clean_text)
                      .apply(add_ending_charecter)
                      .str.replace('  ', ' ')
                      .str.replace(',', ''))
    az['geometry'] = az.geometry.apply(lambda x: [float(i) for i in x.split()])
    az['long'] = az.geometry.apply(lambda x: x[0::2])
    az['lat'] = az.geometry.apply(lambda x: x[1::2])
    az = az.drop('geometry', axis=1)

    data = {'xs': az.long.to_list(), 'ys': az.lat.to_list(),
            'name': az['County Name'].to_list(),
            'workforce': az.workforce.to_list(),
            'unemployment_rate': az.unp_rate.to_list()}

    mapper = LinearColorMapper(palette=Viridis[256],
                               low=min(data['unemployment_rate']),
                               high=max(data['unemployment_rate']))
    color_bar = ColorBar(color_mapper=mapper, label_standoff=10)

    p = figure(title='Arizona Unemployment, 2009', x_axis_location=None,
               y_axis_location=None, tools='hover', toolbar_location=None,
               plot_width=1_000, plot_height=1_000,
               tooltips=[('Name', '@name'), ('Workforce size', '@workforce'),
                         ('Unemployment rate', '@unemployment_rate')])

    p.grid.grid_line_color = None

    p.patches(xs='xs', ys='ys', source=data,
              fill_color={'field': 'unemployment_rate', 'transform': mapper},
              fill_alpha=.8, line_color='lightslategray', line_width=2.5)

    p.add_layout(color_bar, 'right')
    p.title.text_font_size = '24px'

    output_file(DATA_DIR.parent.joinpath('plots').joinpath('04_az_ump.html'),
                title='Arizona Umemployment Rate by County')
    show(p, browser='firefox', )
