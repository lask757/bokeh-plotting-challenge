from pathlib import Path

import matplotlib.pyplot as plt
import numpy as np
from skimage.util import img_as_ubyte
from bokeh.io import show, output_file
from bokeh.plotting import figure
from bokeh.layouts import row


DATA_DIR = Path.cwd().parent.joinpath('data')
URL = 'https://desertpy.com/images/new-desertpy-logo/Logo_DesertPy_ico.png'

with DATA_DIR.joinpath('Logo_DesertPy_ico.png') as img_file:
    img = plt.imread(str(img_file), format='png')

x_dim, y_dim, _ = img.shape
upside_down_image = img_as_ubyte(img)
out_image = np.flip(upside_down_image, axis=0)

fig = {'plot_width': 700, 'plot_height': 700, 'toolbar_location': None,
       'x_range': (0, x_dim), 'y_range': (0, y_dim)}

local = figure(title='From Local Storage', **fig)
local.image_rgba(image=[out_image], x=0, y=0, dw=x_dim, dh=y_dim)

web = figure(title='From Web', **fig)
web.image_url(url=[URL], x=0, y=y_dim, w=x_dim, h=y_dim)

layout = row([local, web])
output_file(DATA_DIR.parent.joinpath('plots').joinpath('07_plot_image.html'),
            title='DesertPy Logo')
show(layout)
