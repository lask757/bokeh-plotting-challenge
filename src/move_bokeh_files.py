import os
from pathlib import Path
import shutil
from typing import Sequence

import bokeh

STOCKS = ('AAPL.csv', 'FB.csv', 'GOOG.csv', 'IBM.csv', 'MSFT.csv')
BOKEH_PATH = Path.home().joinpath('.bokeh/data')
DATA_PATH = Path.cwd().parent.joinpath('data')


def check_if_files_exist(files_to_check: Sequence[str],
                         data_dir: Path) -> bool:
    """
    Args:
        files_to_check: Iterable of items to be checked
        data_dir: location to check for files
    Returns:
        True if all files are in the specified path else false
    """
    if not data_dir.exists():
        data_dir.mkdir()
    d = str(data_dir)
    out = [1 if f in os.listdir(d) else 0 for f in files_to_check]
    return 0 not in out


def copy_files(src: Path, dest: Path, files: Sequence[str]) -> None:
    """
    Args:
        src: location of the files to be copied
        dest: locatian the filse will be copied to
        files: Iterable of files to be copied
    """
    src_files = [src.joinpath(f) for f in files]
    dest_files = [dest.joinpath(f) for f in files]
    for s, d in zip(src_files, dest_files):
        shutil.copy2(s, d)


if __name__ == '__main__':
    in_data_path = check_if_files_exist(STOCKS, DATA_PATH)
    in_bokeh_path = check_if_files_exist(STOCKS, BOKEH_PATH)
    if not in_data_path:
        if in_bokeh_path:
            copy_files(BOKEH_PATH, DATA_PATH, STOCKS)
        else:
            bokeh.sampledata.download()
            copy_files(BOKEH_PATH, DATA_PATH, STOCKS)
