from pathlib import Path
from typing import Iterable, List

import pandas as pd
from bokeh.models import ColumnDataSource, HoverTool
from bokeh.io import show, output_file
from bokeh.plotting import figure

STOCKS = ('AAPL.csv', 'GOOG.csv', 'IBM.csv', 'MSFT.csv')
NAMES = ('Apple', 'Google', 'IBM', 'Microsoft')
COLORS = ('#1f77b4', '#2ca02c', '#d62728', '#9467bd')
DATA_DIR = Path.cwd().parent.joinpath('data')
assert len(STOCKS) == len(NAMES) == len(COLORS)


def load_csvs_to_dataframes(file_names: Iterable[str],
                            location: Path) -> List[pd.DataFrame]:
    """
    Args:
        file_names: Iterable of filenames to return
        location: path to the filenames
    Returns:
        list of pandas dataframes of len(filenames)
    """
    locs = [location.joinpath(f) for f in file_names]
    return \
        [pd.read_csv(i, parse_dates=['Date']) for i in locs]


if __name__ == '__main__':
    dfs = load_csvs_to_dataframes(STOCKS, DATA_DIR)
    cds = [ColumnDataSource(df) for df in dfs]

    hover = HoverTool(tooltips=[('Date', '@Date{%x}'), ('Open', '@Open'),
                                ('High', '@High'), ('Low', '@Low'),
                                ('Close', '@Close'), ('Volume', '@Volume'),
                                ('Adj Close', '@{Adj Close}')],
                      formatters={'Date': 'datetime'})

    p = figure(x_axis_label='Date', x_axis_type='datetime',
               y_axis_label='Adjusted Close', y_axis_type='linear',
               plot_height=1_000, plot_width=1_400, toolbar_location='above',
               title='Historical Stock Prices',
               tools=[hover, 'xwheel_zoom,box_zoom,reset,save'])

    for i, n in enumerate(NAMES):
        p.line('Date', 'Adj Close', source=cds[i], legend_label=n,
               line_color=COLORS[i], line_width=2, muted_color=COLORS[i],
               muted_alpha=.15)

    p.legend.location = 'top_left'
    p.legend.click_policy = 'mute'
    p.title.text_font_size = '24px'
    p.yaxis.axis_label_text_font_size = '18px'
    p.xaxis.axis_label_text_font_size = '18px'

    output_file(DATA_DIR.parent.joinpath('plots')
                .joinpath('01_time_series.html'), title='Time Series Plot')
    show(p, browser='firefox')
