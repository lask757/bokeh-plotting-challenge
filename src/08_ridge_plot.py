from pathlib import Path

import numpy as np
import pandas as pd
from bokeh.io import output_file, show
from bokeh.plotting import figure
from bokeh.palettes import Category20c

DATA_DIR = Path.cwd().parent.joinpath('data')

marriages = pd.read_excel(DATA_DIR.joinpath('mars2019.xlsx'), header=1,
                          na_values='*', skipfooter=3, usecols=[*range(13)],
                          index_col=0)

marriages = marriages.fillna(0)
months = [*marriages.columns]
counties = [*marriages.index]
marriages.loc[:, months].astype(np.float)
marriages_t = marriages.T

p = figure(x_range=months, title='AZ Marriages by Month', x_axis_label='Month',
           y_range=(0, (marriages.loc[:, months].sum().max()) + 500),
           y_axis_label='Count of Marriages', plot_width=1_400,
           plot_height=1_000, toolbar_location=None)

p.varea_stack(stackers=counties, x='index', color=Category20c[len(counties)],
              source=marriages_t, legend_label=counties)

p.legend.location = 'top_center'
p.legend.orientation = 'horizontal'
p.title.text_font_size = '24px'
p.yaxis.axis_label_text_font_size = '18px'
p.xaxis.axis_label_text_font_size = '18px'

output_file(
    DATA_DIR.parent.joinpath('plots').joinpath('08_ridge_area_plot.html'),
    title='AZ Marriages 2019')

show(p, browser='firefox')
