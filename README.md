# Submission to DesertPy's plotting library challenge
#### [Meetup Link](https://www.meetup.com/PyData-Phoenix/events/267759899/), [Github repo](https://github.com/pydata-phoenix/battle-of-the-plotting-libraries)

### 1. [Time Series](https://bokeh-plots.s3.us-east-2.amazonaws.com/plots/01_time_series.html)
#### [Script](https://gitlab.com/lask757/bokeh-plotting-challenge/blob/master/src/01_time_series_plot.py)

### 2. [Categorical Bar Chart](https://bokeh-plots.s3.us-east-2.amazonaws.com/plots/02_stacked_bar.html)
#### [Script](https://gitlab.com/lask757/bokeh-plotting-challenge/blob/master/src/02_categorical_plot.py)

### 3. [Categorical Bar Chart](https://bokeh-plots.s3.us-east-2.amazonaws.com/plots/03_bar_mesa_accidents.html)
#### [Script](https://gitlab.com/lask757/bokeh-plotting-challenge/blob/master/src/03_multi_categorical_plot.py)

### 4. [Geographical Plot](https://bokeh-plots.s3.us-east-2.amazonaws.com/plots/04_az_ump.html)
#### [Script](https://gitlab.com/lask757/bokeh-plotting-challenge/blob/master/src/04_geographical_plot.py)

### 5. [Multivariate Plot](https://bokeh-plots.s3.us-east-2.amazonaws.com/plots/05_multivariate_plot.html)
#### [Script](https://gitlab.com/lask757/bokeh-plotting-challenge/blob/master/src/05_multivariate_plot.py)

### 6. [Muliple KDE Plot](https://bokeh-plots.s3.us-east-2.amazonaws.com/plots/06_multidistrobution_plot.html)
#### [Script](https://gitlab.com/lask757/bokeh-plotting-challenge/blob/master/src/06_multi_distrobution_plot.py)

### 7. [Show PNG images](https://bokeh-plots.s3.us-east-2.amazonaws.com/plots/07_plot_image.html)
#### [Script](https://gitlab.com/lask757/bokeh-plotting-challenge/blob/master/src/07_image_plot.py)

### 8. [Rigde Plot](https://bokeh-plots.s3.us-east-2.amazonaws.com/plots/08_ridge_area_plot.html)
#### [Script](https://gitlab.com/lask757/bokeh-plotting-challenge/blob/master/src/08_ridge_plot.py)

### 9 [Advanced Example - Gapminder (Bokeh Documentation)](https://demo.bokeh.org/gapminder)
